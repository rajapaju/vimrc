#!/bin/bash

command -v git >/dev/null 2>&1 || { echo >&2 "The git is not installed. Aborting."; exit 1; }
mkdir -p $HOME/.vim/bundle
mkdir -p $HOME/.vim/backup
mv $HOME/.vim/* $HOME/.vim/backup/
mv $HOME/.vimrc $HOME/.vim/backup/
ln -s `pwd`/dotvimrc $HOME/.vimrc
git clone https://github.com/gmarik/vundle.git $HOME/.vim/bundle/vundle
vim +BundleInstall +qall
